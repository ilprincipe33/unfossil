Q=@
srcdir = .
exec_prefix = /usr/local
bindir = $(exec_prefix)/bin

prefix = /usr/local/unfossil
includedir = $(prefix)/include
libdir = $(prefix)/include
libdir = $(prefix)/lib

SRC_DIRS = $(addprefix $(srcdir),src/* )
SRCS = $(foreach d,$(SRC_DIRS),$(wildcard $(addprefix $(d)/*,.c)))
SRCS += src/unfossil.c src/hexdump.c src/cryptonight.c src/net.c
OBJS = $(addsuffix .o, $(basename $(SRCS)))

CC = gcc
LDFLAGS = 
CFLAGS = -Wall -Wmissing-prototypes -Wstrict-prototypes -O2 \
		 -fomit-frame-pointer -std=gnu89

SUBARCH = $(shell uname -m | sed -e s/i.86/x86/ -e s/x86_64/x86/ \
								  -e s/sun4u/sparc64/ \
								  -e s/arm.*/arm/ -e s/sa110/arm/ \
								  -e s/s390x/s390/ -e s/parisc64/parisc/ \
								  -e s/ppc.*/powerpc/ -e s/mips.*/mips/ \
								  -e s/sh[234].*/sh/ -e s/aarch64.*/arm64/ \
								  -e s/riscv.*/riscv/)

-include config.mk

TARGET = unfossil

all: $(TARGET)

CC_CMD = $(CC) $(CFLAGS) -c -o $@ $<

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS)

$(OBJ_DIRS):
	mkdir -p $@

clean:
	$(Q)rm -rf obj $(TARGET) $(OBJS)

install:
	@echo $(SRCS)
	@echo $(OBJS)

help:
	@echo 'Cleaning targets:'
	@echo '	clean			 - Remove most generated files except for config '

.PHONY: all clean help
