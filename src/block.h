#ifndef BLOCK_H
#define BLOCK_H

struct blk_hdr {
	uint8_t maj_ver;
	uint8_t min_ver;
	char prev_id[32]; /* HASH_SIZE in cryptonight.h */
	uint64_t timestamp;
	uint32_t nonce;
};

#endif /* BLOCK_H */
