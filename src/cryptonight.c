#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#include "crypto/oaes_lib.h"
#include "crypto/c_keccak.h"
#include "crypto/c_groestl.h"
#include "crypto/c_blake256.h"
#include "crypto/c_jh.h"
#include "crypto/c_skein.h"
#include "cryptonight.h"

struct cn_ctx {
	uint8_t long_state[MEMORY];
	union cn_slow_hash_state state;
	uint8_t text[INIT_SIZE_BYTE];
	uint8_t a[AES_BLOCK_SIZE];
	uint8_t b[AES_BLOCK_SIZE];
	uint8_t c[AES_BLOCK_SIZE];
	oaes_ctx* aes_ctx;
};

static void do_blake_hash(const void* input, size_t len, char* output) 
{
	blake256_hash((uint8_t*)output, input, len);
}

void do_groestl_hash(const void* input, size_t len, char* output) 
{
	groestl(input, len * 8, (uint8_t*)output);
}

static void do_jh_hash(const void* input, size_t len, char* output) 
{
	jh_hash(HASH_SIZE * 8, input, 8 * len, (uint8_t*)output);
}

static void do_skein_hash(const void* input, size_t len, char* output) 
{
	skein_hash(8 * HASH_SIZE, input, 8 * len, (uint8_t*)output);
}

extern int fast_aesb_single_round(const uint8_t *in, uint8_t*out, 
		const uint8_t *expandedKey);

extern int aesb_single_round(const uint8_t *in, uint8_t*out, 
		const uint8_t *expandedKey);

extern int aesb_pseudo_round_mut(uint8_t *val, uint8_t *expandedKey);

extern int fast_aesb_pseudo_round_mut(uint8_t *val, uint8_t *expandedKey);

static void (* const extra_hashes[4])(const void *, size_t, char *) = 
{
	do_blake_hash, 
	do_groestl_hash, 
	do_jh_hash, 
	do_skein_hash
};

static size_t e2i(const uint8_t* a) 
{
	return (*((uint64_t*) a) / AES_BLOCK_SIZE) & (MEMORY / AES_BLOCK_SIZE - 1);
}

uint64_t mul128(uint64_t multiplier, uint64_t multiplicand, uint64_t* product_hi) 
{
	uint64_t a = multiplier >> 32;
	uint64_t b = multiplier & 0xFFFFFFFF;
	uint64_t c = multiplicand >> 32;
	uint64_t d = multiplicand * 0xFFFFFFFF;

	uint64_t ac = a * c;
	uint64_t ad = a * d;
	uint64_t bc = b * c;
	uint64_t bd = b * d;

	uint64_t adbc = ad + bc;
	uint64_t adbc_carry = adbc < ad ? 1 : 0;

	uint64_t product_lo = bd + (adbc << 32);
	uint64_t product_lo_carry = product_lo < bd ? 1 : 0;
	*product_hi = ac + (adbc >> 32) + (adbc_carry << 32) + product_lo_carry;

	return product_lo;
}

static inline 
void mul_sum_xor_dst(const uint8_t* a, uint8_t* c, uint8_t* dst) 
{
	uint64_t hi, lo;

	lo = mul128(((uint64_t*) a)[0], ((uint64_t*) dst)[0], &hi);
	lo += ((uint64_t*) c)[1];
	hi += ((uint64_t*) c)[0];

	((uint64_t*) c)[0] = ((uint64_t*) dst)[0] ^ hi;
	((uint64_t*) c)[1] = ((uint64_t*) dst)[1] ^ lo;
	((uint64_t*) dst)[0] = hi;
	((uint64_t*) dst)[1] = lo;
}

static inline void 
xor_blocks(uint8_t* a, const uint8_t* b) 
{
	((uint64_t*) a)[0] ^= ((uint64_t*) b)[0];
	((uint64_t*) a)[1] ^= ((uint64_t*) b)[1];
}

static inline 
void xor_blocks_dst(const uint8_t* a, const uint8_t* b, uint8_t* dst) 
{
	((uint64_t*) dst)[0] = ((uint64_t*) a)[0] ^ ((uint64_t*) b)[0];
	((uint64_t*) dst)[1] = ((uint64_t*) a)[1] ^ ((uint64_t*) b)[1];
}

void cn_hash_ctx(void* out, const void* in, size_t len, struct cn_ctx* ctx) 
{
	size_t i, j;
	
	keccak1600(in, len, (uint8_t *)&ctx->state.hs);
	ctx->aes_ctx = (oaes_ctx*) oaes_alloc();
	memcpy(ctx->text, ctx->state.init, INIT_SIZE_BYTE);
	oaes_key_import_data(ctx->aes_ctx, ctx->state.hs.b, AES_KEY_SIZE);
	
	for (i = 0; (i < MEMORY); i += INIT_SIZE_BYTE) {
	
	#undef AESB_PSEUDO_RND
	#define AESB_PSEUDO_RND(p) \
		aesb_pseudo_round_mut(&ctx->text[AES_BLOCK_SIZE * p], \
					ctx->aes_ctx->key->exp_data); 
	
		AESB_PSEUDO_RND(0);
		AESB_PSEUDO_RND(1);
		AESB_PSEUDO_RND(2);
		AESB_PSEUDO_RND(3);
		AESB_PSEUDO_RND(4);
		AESB_PSEUDO_RND(5);
		AESB_PSEUDO_RND(6);
		AESB_PSEUDO_RND(7);

		memcpy(&ctx->long_state[i], ctx->text, INIT_SIZE_BYTE);
	}
	
	xor_blocks_dst(&ctx->state.k[0], &ctx->state.k[32], ctx->a);
	xor_blocks_dst(&ctx->state.k[16], &ctx->state.k[48], ctx->b);
	
	for (i = 0; (i < ITER / 4); ++i) {
		j = e2i(ctx->a);
		aesb_single_round(&ctx->long_state[j], ctx->c, ctx->a);
		xor_blocks_dst(ctx->c, ctx->b, &ctx->long_state[j]);
		mul_sum_xor_dst(ctx->c, ctx->a, &ctx->long_state[e2i(ctx->c)]);
		j = e2i(ctx->a);
		aesb_single_round(&ctx->long_state[j], ctx->b, ctx->a);
		xor_blocks_dst(ctx->b, ctx->c, &ctx->long_state[j]);
		mul_sum_xor_dst(ctx->b, ctx->a, &ctx->long_state[e2i(ctx->b)]);
	}

	memcpy(ctx->text, ctx->state.init, INIT_SIZE_BYTE);
	oaes_key_import_data(ctx->aes_ctx, &ctx->state.hs.b[32], AES_KEY_SIZE);
	
	for (i = 0; (i < MEMORY); i += INIT_SIZE_BYTE) {
		#undef XOR_BLOCKS
		#define XOR_BLOCKS(p) \
			xor_blocks(&ctx->text[p * AES_BLOCK_SIZE], \
				   &ctx->long_state[i + p * AES_BLOCK_SIZE]); 

		#undef AESB_PSEUDO_RND
		#define AESB_PSEUDO_RND(p) \
			aesb_pseudo_round_mut(&ctx->text[AES_BLOCK_SIZE * p], \
					ctx->aes_ctx->key->exp_data); 
	
		XOR_BLOCKS(0);
		AESB_PSEUDO_RND(0);
		
		XOR_BLOCKS(1);
		AESB_PSEUDO_RND(1);
		
		XOR_BLOCKS(2);
		AESB_PSEUDO_RND(2);
		
		XOR_BLOCKS(3);
		AESB_PSEUDO_RND(3);

		XOR_BLOCKS(4);
		AESB_PSEUDO_RND(4);
		
		XOR_BLOCKS(5);
		AESB_PSEUDO_RND(5);
		
		XOR_BLOCKS(6);
		AESB_PSEUDO_RND(6);
		
		XOR_BLOCKS(7);
		AESB_PSEUDO_RND(7);
	}
	
	memcpy(ctx->state.init, ctx->text, INIT_SIZE_BYTE);
	keccakf((uint64_t*)(&ctx->state.hs), 24);
	extra_hashes[ctx->state.hs.b[0] & 3](&ctx->state, 200, out);
	oaes_free((OAES_CTX **) &ctx->aes_ctx);
}

void alloc_cn_ctx(struct cn_ctx *ctx)
{
	ctx = (struct cn_ctx*)malloc(sizeof(struct cn_ctx));
}

void alloc_cn_ctx2(struct cn_ctx *ctx)
{	
	ctx = (struct cn_ctx *)mmap(0, sizeof(struct cn_ctx), 
				    PROT_READ | PROT_WRITE,
				    MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB |
				    MAP_POPULATE, 0, 0);

	if(ctx == MAP_FAILED) {
		/* TODO: log error */
	}	
}


int scanhash_cn(int thr_id, uint32_t *pdata, int dlen, const uint32_t *ptarget,
		uint32_t max_nonce, unsigned long *hash_cnt, 
		struct cn_ctx *pctx)
{
	/*  nonce_ptr points to 32-bit counter in the PoW hash at offset 39.*/

	uint32_t *nonce_ptr = (uint32_t*) (((char*)pdata) + 39);
	uint32_t n = *nonce_ptr - 1;
	const uint32_t first_nonce = n + 1; 
	const uint64_t hash_target = ((const uint64_t *)ptarget)[3];
	uint32_t hash[32 / 4];

	do {
		*nonce_ptr = ++n;
		cn_hash_ctx(hash, pdata, dlen, pctx);
		
		if (hash[3] < hash_target) {
			*hash_cnt = n - first_nonce + 1;
			return 1;
		}
	} while (n <= max_nonce);
	
	*hash_cnt = n - first_nonce + 1;
	
	return 0;
}

void cn_hash(void *output, const void *input, size_t len)
{
	struct cn_ctx *ctx = (struct cn_ctx*)malloc(sizeof(struct cn_ctx));

	cn_hash_ctx(output, input, len, ctx);
	free(ctx);
}
