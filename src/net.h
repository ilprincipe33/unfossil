#ifndef NET_H
#define NET_H

#define NET_ERR -1 
#define NET_OK 0 

#define NET_ERR_LEN 256
#define BACKLOG_SIZE 64

int tcp_keepalive(int fd, int intvl);
int tcp_setblock(int fd, int non_block);
int tcp_nodelay(int fd, int no_delay);
int tcp_sendbuf(int fd, int sz);
int tcp_sendtimeout(int fd, long long ms);

int tcp_server(int port, char *host);
int tcp_accept(int s, char *ip, size_t ip_len, int *port);
int tcp_connect(char *addr, int port);

#endif /* NET_H */
