#ifndef MTHREAD_H
#define MTHREAD_H

#include <pthread.h>
#include "llist.h"

#ifndef CONFIG_NR_CPUS
#define CONFIG_NR_CPUS 1
#endif

#define NR_CPUS CONFIG_NR_CPUS

struct mthread_create_info {
	int (*threadfn)(void *data);
	void *data;
};

struct mthread {
	unsigned long flags;
	unsigned long int cpu;
	void *data; 
	pthread_t pthr; 
};


#endif /* MTHREAD_H */
