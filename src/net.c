#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>

#include "net.h"

static void net_seterr(char *err, const char *fmt, ...)
{
	va_list ap;
	if (!err) 
		return;
	
	va_start(ap, fmt);
	vsnprintf(err, NET_ERR_LEN, fmt, ap);
	va_end(ap);
}

int tcp_setblock(int fd, int non_block) 
{
	int flags;

	if ((flags = fcntl(fd, F_GETFL)) == -1)
		return NET_ERR;

	if (non_block)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;

	if (fcntl(fd, F_SETFL, flags) == -1)
		return NET_ERR;
	
	return NET_OK;
}

/* set the TCP keep alive option to detect dead peers. */
int tcp_keepalive(int fd, int intvl) 
{
	int v = 1; 

	if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &v, sizeof(v)) == -1)
		return NET_ERR;
	
	v = intvl; 

	if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPIDLE, &v, sizeof(v)) < 0)
		return NET_ERR; 

	v = intvl / 3;	
	if (v == 0) 
		v = 1;

	if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPINTVL, &v, sizeof(v)) < 0) 
		return NET_ERR;

	v = 3;
	if (setsockopt(fd, IPPROTO_TCP, TCP_KEEPCNT, &v, sizeof(v)) < 0)
		return NET_ERR;

	return NET_OK;
}

int tcp_nodelay(int fd, int val)
{
	if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val)) == -1)
		return NET_ERR;
	
	return NET_OK;
}

/*
 * set the socket timeout (SO_SNDTIMEO option) to the specified number of 
 * milliseconds or disable it if 'ms' is zero
 */
int tcp_sendtimeout(int fd, long long ms)
{
	struct timeval tv;

	tv.tv_sec = ms / 1000;
	tv.tv_usec = (ms % 1000 ) * 1000; 
	
	if (setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) == -1) 
		return NET_ERR;

	return NET_OK;
}

int tcp_sendbuf(int fd, int sz)
{
	if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, &sz, sizeof(sz)) == -1)
		return NET_ERR;

	return NET_OK;
}

static int tcp_listen(int s, struct sockaddr *sa, socklen_t len) 
{
	if (bind(s, sa, len) == -1) {
		close(s);
		return NET_ERR;
	}
	
	if (listen(s, BACKLOG_SIZE) == -1) {
		close(s);
		return NET_ERR;
	}
	return NET_OK;
}

static int tcp_v6only(int s) 
{
	int f = 1;
	if (setsockopt(s, IPPROTO_IPV6, IPV6_V6ONLY, &f, sizeof(f)) == -1) {
		close(s);
		return NET_ERR;
	}
	return NET_OK;
}

static int tcp_set_reuseaddr(int fd) 
{
	int f = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &f, sizeof(f)) == -1)
		return NET_ERR;
	return NET_OK;
}

static int __tcp_server(int port, char *host, int af)
{
	int s = NET_ERR, rv; 
	char portstr[6];

	struct addrinfo hints, *servinfo, *p;

	snprintf(portstr, 6, "%d", port); 
	memset(&hints, 0, sizeof(hints)); 
	hints.ai_family = af; 
	hints.ai_socktype = SOCK_STREAM; 
	hints.ai_flags = AI_PASSIVE; 

	if ((rv = getaddrinfo(host, portstr, &hints, &servinfo)) != 0)
		return NET_ERR; 
	
	for (p = servinfo; p != NULL; p = p->ai_next) {
		s = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (s == -1)
			continue;

		if (af == AF_INET6 && tcp_v6only(s) == NET_ERR)
			goto err;

		if (tcp_set_reuseaddr(s) == NET_ERR)
			goto err;

		if (tcp_listen(s, p->ai_addr, p->ai_addrlen) == NET_ERR)
			goto err;

		goto out;
	}
		
	if (p == NULL) {
		fprintf(stderr, "unable to bind socket errno: %d", errno);
		goto err;
	}
err:
	if (s != -1)
		close(s);
	s = NET_ERR;

out:
	freeaddrinfo(servinfo);
	printf("it worked!\n");
	return s; 
}

int tcp_server(int port, char *host)
{
	return __tcp_server(port, host, AF_INET);
}

static int generic_accept(int s, struct sockaddr *sa, socklen_t *len) 
{
	int fd;
	while(1) {
		fd = accept(s, sa, len);
		if (fd < 0) {
			switch (errno) {
			case EAGAIN:
			case EINTR:
			case ECONNABORTED:
				continue;
			default:
				return NET_ERR;
			}
		}
		break;
	}
	return fd;
}

int tcp_accept(int s, char *ip, size_t ip_len, int *port) 
{
	int fd; 
	struct sockaddr_storage sa; 
	socklen_t sa_len = sizeof(sa);
	fd = generic_accept(s, (struct sockaddr*)&sa, &sa_len);
	if (fd == -1)
		return NET_ERR;

	if (sa.ss_family == AF_INET) {
		struct sockaddr_in *s = (struct sockaddr_in *)&sa; 
		if (ip)
			inet_ntop(AF_INET, (void*)&(s->sin_addr), ip, ip_len);
		
		if (port)
			*port = ntohs(s->sin_port);
		
	} else {
		struct sockaddr_in6 *s = (struct sockaddr_in6 *)&sa; 
		if (ip)
			inet_ntop(AF_INET6, (void*)&(s->sin6_addr), ip, ip_len);
		
		if (port)
			*port = ntohs(s->sin6_port);
	}

	return fd;
}

#define NET_CONN_NONE 0
#define NET_CONN_NONBLOCK 1
#define NET_CONN_BE_BINDING 2
static int __tcp_connect(char *addr, int port, char *src_addr, int flags)
{
	int s = NET_ERR, rv;
	char portstr[6];
	struct addrinfo hints, *servinfo, *bservinfo, *p, *b;

	snprintf(portstr, sizeof(portstr), "%d", port);
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(addr, portstr, &hints, &servinfo)) != 0)
		return NET_ERR;

	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((s = socket(p->ai_family, p->ai_socktype, 
				p->ai_protocol)) == -1)
			continue;
		
		if (tcp_set_reuseaddr(s) == NET_ERR) 
			goto err;
		
		if (flags & NET_CONN_NONBLOCK && tcp_setblock(s, 0) != NET_OK)
			goto err;

		if (src_addr) {
			int bound = 0;
			
			if ((rv = getaddrinfo(src_addr, NULL, &hints, 
					      &bservinfo)) != 0)
				goto err;

			for (b = bservinfo; b != NULL; b = b->ai_next) {
				if (bind(s, b->ai_addr, b->ai_addrlen) != -1) {
					bound = 1;
					break;
				}
			}
			freeaddrinfo(bservinfo);
			if (!bound) 
				goto err;
		}

		if (connect(s, p->ai_addr, p->ai_addrlen) == -1) {
			if (errno == EINPROGRESS && flags & NET_CONN_NONBLOCK)
				goto end;
			close(s);
			s = NET_ERR;
			continue;
		}

		goto end;
	}
	if (p == NULL)
		return NET_ERR; 

err:
	if (s != NET_ERR) {
		close(s);
		s = NET_ERR;
	}
end:
	freeaddrinfo(servinfo);
	return s;
}

int tcp_connect(char *addr, int port)
{
	return __tcp_connect(addr, port, NULL, NET_CONN_NONE);
}
