#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <sys/types.h>

int hex_to_bin(char ch)
{
	if ((ch >= '0') && (ch <= '9'))
		return ch - '0';
	ch = tolower(ch);
	if ((ch >= 'a') && (ch <= 'f'))
		return ch - 'a' + 10;
	return -1;
}

int hex2bin(uint8_t *dst, const char *src, size_t count)
{
	while (count--) {
		int hi = hex_to_bin(*src++);
		int lo = hex_to_bin(*src++);

		if ((hi < 0) || (lo < 0))
			return -EINVAL;


		*dst++ = (hi << 4) | lo;
	}
	return 0;
}


char *bin2hex(char *dst, const void *src, size_t count) 
{
	return 0;
}
