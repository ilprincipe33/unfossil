#ifndef CRYPTONIGHT_H
#define CRYPTONIGHT_H

#define MEMORY         (1 << 21) // 2 MiB / 2097152 B
#define ITER           (1 << 20) // 1048576
#define AES_BLOCK_SIZE  16
#define AES_KEY_SIZE    32
#define INIT_SIZE_BLK   8
#define INIT_SIZE_BYTE (INIT_SIZE_BLK * AES_BLOCK_SIZE) // 128 B

#ifndef HASH_SIZE
#define HASH_SIZE 32
#endif

union hash_state {
    uint8_t b[200];
    uint64_t w[25];
};

union cn_slow_hash_state {
    union hash_state hs; 
    struct {
        uint8_t k[64];
        uint8_t init[INIT_SIZE_BYTE];
    };
};

struct cn_ctx; 

void init_cn_ctx(uint8_t *hp_state);
void cn_hash(void* out, const void* in, size_t len);

#endif /* CRYPTONIGHT_H */
