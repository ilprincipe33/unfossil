#ifndef HASH_H
#define HASH_H
#pragma once

typedef unsigned char BitSequence;
typedef unsigned long long DataLength;
typedef enum {SUCCESS = 0, FAIL = 1, BAD_HASHLEN = 2} HashReturn;

#endif /* HASH_H */
